<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/xenon-core.css">
    <link rel="stylesheet" href="/css/xenon-forms.css">
    <link rel="stylesheet" href="/css/xenon-components.css">
    <link rel="stylesheet" href="/css/xenon-skins.css">
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="/css/custom.css">
    <script src="{{ asset('/js/app.js') }}"></script>
    <script  src="/js/bootstrap-datepicker.js"></script>
    <script  src="/js/daterangepicker.js"></script>
    <script src="/js/toastr.min.js"></script>


    {{--<script src="{{ asset('/js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/js/') }}"></script>
--}}

    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-inverse" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{route('home')}}">Sancharam</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{route('login')}}">Book flight</a>
                    </li>
                    <li>
                        <a href="{{route('login')}}">Book Bus</a>
                    </li>


                </ul>

                @if(Auth::check())
                    <ul class="nav navbar-nav navbar-right">

                         <form action="{{url('logout')}}" class="btn btn-red btn-icon btn-icon-standalone" method="POST">
                             {{ csrf_field() }}
                                <input type="submit" class="button btn btn-danger" value="Logout">
                            </form>
                    </ul>
                @else
                <ul class="nav navbar-nav navbar-right">

                    <li> <a href="{{route('register')}}" class="btn btn-red btn-icon btn-icon-standalone">
                            <i class="fa-pencil-square-o"></i>
                            <span>Signup</span>
                        </a>
                    </li>

                    <li>
                        <a class="btn btn-info btn-icon-standalone" href="{{route('login')}}">
                            <i class="fa-sign-in"></i>
                            <span>login</span>
                        </a>
                    </li>
                    @endif
                    <!--<button class="btn btn-red"> <a href="{{route('login')}}"> Login </a></button>
            <button class="btn btn-purple"> <a href= "{{route('register')}}"> Sigh up </a></button>-->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>


        @yield('content')
    </div>
</body>
</html>
