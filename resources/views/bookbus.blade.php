@extends('layouts.app')
@section('content')
    <script src="/js/bootstrap-datepicker.js"></script>
    <script src="/js/daterangepicker.js"></script>
    <div class="row">
        <div class="col-sm-12" >

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 style = "align:center; vertical-align:middle;"  align="center">Online Bus Ticket Booking</h2>

                </div>
                <div class="panel-body">
                    {{-- search form --}}
                    <form role="form" class="form-inline" 
                    action="{{ route('searchBus') }}" method="GET">

                        <div class="form-group">

                            <input name="from"  id="to" type="text" class="form-control" size="25" placeholder="From">
                        </div>

                        <div class="form-group">

                            <input name="to" type="text" id="from" class="form-control" size="25" placeholder="To">
                        </div>



                        <div class="form-group">


                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" name="onwarddate" placeholder="Onward Date" class="form-control datepicker" data-format="D, dd MM yyyy">

                                    <div class="input-group-addon">
                                        <a href="#"><i class="linecons-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">


                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" name="returndate" placeholder="Return Date" class="form-control datepicker" data-format="D, dd MM yyyy">

                                    <div class="input-group-addon">
                                        <a href="#"><i class="linecons-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <button class="btn btn-secondary btn-single" style="height:35px;width:250px" >Search</button>
                        </div>

                    </form>
                    <hr>

                </div>
            </div>

        </div>
    </div>
<!--End of search-->
    <style>
    #bookinfo i{
        font-size:25px;
    }
    </style>

    <h2 style = "align:center; vertical-align:middle;"  align="center" > Buses</h2>
<!--Result showing-->
    @if($bookings == null)
        <div class="page-error centered">
            <hr>
            <h1 class="text-center warning"><i class="fa-warning"></i> No results found :(</h1>
            <hr>
        </div>           
    @else
    @foreach($bookings as $booking)
    <div class="col-sm-6 col-md-3 col-xs-12 grid-item">
        <div class="panel panel-default">

            <div class="panel-body" id="bookinfo">
                <div class="user-info-sidebar">
                    <a href="#" class="user-img">
                        <img src="{{ route('serveImage',['id' => $booking->image]) }}" alt="user-img" class="img-cirlce img-responsive
                                img-thumbnail ride_pic">
                    </a>
                    <br>
                    <a href="#" class="user-name">
                        <h3 class="">{{ $booking->busname }}</h3>
                        <span class="user-status is-online"></span>
                    </a>


                    <hr>

                    <ul class="list-unstyled user-info-list">

                        <li>
                            <img src="/images/transfer.png">
                            <strong><span>{{$booking->from}}</span></strong> <br><span class="text-center"><i class="fa-long-arrow-right"></i></span>
                            <strong><span>{{$booking->to}}</span></strong>
                        </li>
                        <br>
                        <li>
                            <img src="/images/calendar.png">


                            <strong><span>{{$booking->depdate}} </span></strong>
                        </li>
                        <li>
                            <img src="/images/calendar.png">
                            <strong><span>{{$booking->arivaldate}} </span></strong>
                        </li>
                        <br>
                        <li>
                            <img src="/images/clock.png">
                            <strong><span>{{ $booking->time }}</span></strong>
                        </li>
                        <br>

                         <img src="/images/rupee.png">
                        <strong>{{ $booking->rate }}</strong>

                        <br>
                        <br>

                    </ul>
                    <button class="btn btn-primary btn-icon btn-icon-standalone" id="phnbtn1">
                        <i class="fa fa-phone-square"></i>
                        <span>{{ $booking->contact }}</span>
                    </button>
                </div></div>
            <div class="btn-group btn-group-justified">
                <a class="btn btn-success btn-sm" href="{{ route('newbusbooking',['id'=>$booking->id]) }}">Book Now
                    <i class="fa-plus-circle"> </i>
                </a>
            </div>
        </div>
    </div>
    @endforeach
    @endif
{{--result end--}}
    <script>
        $('.datepicker').datepicker();
    </script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAsIHZOFZAi1kiOgSPaQ4m0Q0Glb8PVPbE&libraries=places"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('to'));
            var places2 = new google.maps.places.Autocomplete(document.getElementById('from'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;

            });

            google.maps.event.addListener(places2, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
            });

        });
    </script>
    @if(Session::has('success'))
        <script>
            toastr.success('Ride saved!','success');
        </script>
    @elseif(Session::has('error'))
        <script>
            toastr.error('Booking failed','Oops!');
        </script>
    @elseif(Session::has('duplicate'))
        <script>
            toastr.warning('You have already booked for this ride','duplicate Booking');
        </script>
    @endif
@endsection