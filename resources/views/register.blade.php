@extends('layout.navbar')
@section('content')

    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-4 col-sm-offset-4" >

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">SANCHARAM</h3>
                    <div class="panel-options">

                    </div>
                </div>
                <div class="panel-body">

                    <form role="form" action="{{route('regsubmit')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">

                            <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                        </div>

                        <div class="form-group">

                            <input type="email" name="email" class="form-control" id="email-1" placeholder="Email…">
                        </div>

                        <div class="form-group">

                            <input type="text" name="mobile" class="form-control" id="Mobile" placeholder="Mobile No">
                        </div>
                        <div class="form-group">

                            <input type="file" name="propic" class="form-control" id="password-1"
                                   placeholder="Profile Pic">
                        </div>
                        <div class="form-group">

                            <input type="password" name="password" class="form-control" id="password-1"
                                   placeholder="password">
                        </div>

                        <div class="form-group">

                            <input type="password" class="form-control" id="password-1"
                                   placeholder="Confirm Password">
                        </div>



                        <div class="form-group">

                            <input type="submit" value="Register now" class="btn btn-info btn-single pull-right">
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection