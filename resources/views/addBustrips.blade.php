@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 style="align:center; vertical-align:middle;" align="center">Add Rides</h1>

                </div>

                <div class="panel-body">

                    <form role="form" id="tripform" class="form-horizontal" enctype="multipart/form-data" action="{{route('postbustrip')}}"
                          method="post">
{{--
                        @if (count($errors) > 0)

                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif--}}

                            {{ csrf_field() }}
                                    <!--Bus name-->
                            <div class="form-group @if($errors->has('busname'))  validate-has-error @endif">
                                <label class="col-sm-2 control-label" for="field-1">Bus Name</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{old('busname')}}" name="busname" class="form-control" id="field-1"
                                           placeholder="Eg: kallada Travels">
                                    @if($errors->has('busname'))
                                        <span id="url-error"
                                              class="validate-has-error">{{ $errors->first('busname') }}</span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group-separator"></div>

                            <!--Select bus type-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Select bus type</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="bustype">
                                        <option value="1">volvo</option>
                                        <option value="2">Superfast</option>
                                        <option value="3">Ordinary</option>
                                        <option value="4">sleeper</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group-separator"></div>
                            <!--Bus Image-->
                            <div class="form-group @if($errors->has('image'))  validate-has-error @endif" {{--@if($errors->has('image'))  validate-has-error @endif--}}>
                                <label class="col-sm-2 control-label" for="field-4">Bus Image</label>

                                <div class="col-sm-10">
                                    <input name="image" type="file"  accept="image/x-png,image/gif,image/jpeg" class="form-control" id="field-4">
                                </div>

                                @if($errors->has('image'))
                                    <span id="url-error"
                                          class="validate-has-error">{{ $errors->first('image') }}</span>
                                @endif
                            </div>

                            <div class="form-group-separator"></div>
                            <!--From-->
                            <div class="form-group @if($errors->has('from'))  validate-has-error @endif ">
                                <label class="col-sm-2 control-label" for="field-1">From</label>

                                <div class="col-sm-10">
                                    <input type="text"  value="{{old('from')}}" name="from" id="from" class="form-control" id="field-1"
                                           placeholder="Eg: Amritapuri">

                                    @if($errors->has('from'))
                                        <span id="url-error"
                                              class="validate-has-error">{{ $errors->first('from') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group-separator"></div>
                            <!--To-->
                            <div class="form-group  @if($errors->has('to'))  validate-has-error @endif">
                                <label class="col-sm-2 control-label" for="field-1">To</label>

                                <div class="col-sm-10">
                                    <input type="text"  value="{{old('to')}}"  name="to" id="to" class="form-control" id="field-1"
                                           placeholder="Eg: Banglore">

                                    @if($errors->has('to'))
                                        <span id="url-error"
                                              class="validate-has-error">{{ $errors->first('to') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group-separator"></div>

                            <!--Waypoints-->
                            <!-- <div class="col-md-12">
                                 <div class="form-group text-right">
                                     <label class="control-label" for="waypoints">In between stops</label>
                                     <input class="form-control" name="waypoints" id="stop_points" placeholder="Ex:kottayam" autocomplete="off" aria-invalid="false">
                                     <br>
                                     <input type="button" id="add_stop" value="add" class="button btn-primary btn">
                                     <div id="stop_tags">
                                         <div class="clearfix"></div>
                                     </div>
                                 </div>
                             </div>-->
                            {{--departure date--}}
                            <div class="form-group  @if($errors->has('depdate'))  validate-has-error @endif">
                                <label class="col-sm-2 control-label" for="field-1">Departure Date</label>

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="text" value="{{old('depdate')}}" name="depdate" placeholder="Departure Date"
                                               class="form-control datepicker" data-format="D, dd MM yyyy">

                                        <div class="input-group-addon">
                                            <a href="#"><i class="linecons-calendar"></i></a>
                                        </div>

                                        @if($errors->has('depdate'))
                                            <span id="url-error"
                                                  class="validate-has-error">{{ $errors->first('depdate') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-separator"></div>
                            {{--Arrival date--}}
                            <div class="form-group  @if($errors->has('arivaldate'))  validate-has-error @endif">

                                <label class="col-sm-2 control-label" for="field-1">Arrival Date</label>

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="text"   value="{{old('arivaldate')}}"  name="arivaldate" placeholder="Arrival Date"
                                               class="form-control datepicker" data-format="D, dd MM yyyy">

                                        <div class="input-group-addon">
                                            <a href="#"><i class="linecons-calendar"></i></a>
                                        </div>

                                        @if($errors->has('arivaldate'))
                                            <span id="url-error"
                                                  class="validate-has-error">{{ $errors->first('arivaldate') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-separator"></div>
                            <!--Time-->
                            <div class="form-group @if($errors->has('time'))  validate-has-error @endif">
                                <label class="col-sm-2 control-label" for="field-1">Time</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{old('time')}}" name="time" class="form-control" id="field-1"
                                           placeholder="Eg:9:30 pm ">
                                </div>

                                @if($errors->has('arivaldate'))
                                    <span id="url-error"
                                          class="validate-has-error">{{ $errors->first('time') }}</span>
                                @endif
                            </div>

                            <div class="form-group-separator"></div>
                            <!--Rate-->
                            <div class="form-group @if($errors->has('rate'))  validate-has-error @endif">
                                <label class="col-sm-2 control-label" for="field-1">Rate</label>

                                <div class="col-sm-10">
                                    <input type="text"  value="{{old('rate')}}" name="rate" class="form-control" id="field-1"
                                           placeholder="Eg:2000 ">
                                </div>

                                @if($errors->has('rate'))
                                    <span id="url-error"
                                          class="validate-has-error">{{ $errors->first('rate') }}</span>
                                @endif
                            </div>

                            <div class="form-group-separator"></div>
                            <!--Contact No-->
                            <div class="form-group @if($errors->has('contact'))  validate-has-error @endif">
                                <label class="col-sm-2 control-label" for="field-1">Contact No:</label>

                                <div class="col-sm-10">
                                    <input type="text"  value="{{old('contact')}}" name="contact" class="form-control" id="field-1"
                                           placeholder="Eg:9947152402 ">
                                </div>
                                @if($errors->has('contact'))
                                    <span id="url-error"
                                          class="validate-has-error">{{ $errors->first('contact') }}</span>
                                @endif
                            </div>
                            <div class="form-group-separator"></div>


                    </form>
                    <button onclick=" $('#tripform').unbind('submit').submit() " class="btn btn-primary btn-lg btn-block">Submit</button>

                </div>
            </div>

        </div>
    </div>
    <script>
        $('.datepicker').datepicker();
        $(function () {
            $("#tripform").submit(function (e) {
                e.preventDefault();
            });


        })


    </script>
     <script src="/js/toastr.min.js"></script>
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAsIHZOFZAi1kiOgSPaQ4m0Q0Glb8PVPbE&libraries=places"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('to'));
            var places2 = new google.maps.places.Autocomplete(document.getElementById('from'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;

            });

            google.maps.event.addListener(places2, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
            });

        });

    </script>
    @if(Session::has('success'))
        <script>
            toastr.success('Ride booked!','success');
        </script>
    @endif
@endsection