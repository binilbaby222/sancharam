@extends('layout.navbar')
@section('content')

    <div class="row" >
        <div class="col-sm-4 col-sm-offset-4" >

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">SANCHARAM</h3>
                    <div class="panel-options">
                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">–</span>
                            <span class="expand-icon">+</span>
                        </a>
                        <a href="#" data-toggle="remove">
                            ×
                        </a>
                    </div>
                </div>
                <div class="panel-body">

                    <form role="form">

                        <div class="form-group">

                            <input type="email" class="form-control" id="email-1" placeholder="Enter your email…">
                        </div>

                        <div class="form-group">

                            <input type="password" class="form-control" id="password-1"
                                   placeholder="Enter your password">
                        </div>

                        <div class="form-group">
                            <label>
                                <div class="cbr-replaced cbr-checked">
                                    <div class="cbr-input"><input type="checkbox" class="cbr cbr-done" checked=""></div>
                                    <div class="cbr-state"><span></span></div>
                                </div>
                                Remember me next time
                            </label>
                        </div>

                        <div class="form-group">

                            <button type="button" class="btn btn-info btn-single pull-right">Login</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection