<?php

namespace App\Http\Controllers;

use App\bustrip;
use Illuminate\Support\Facades\Auth;
use App\busbooking;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function home()
    {

        return view('home');
    }

    function login()
    {

        return view('login');
    }

    function register()
    {

        return view('register');
    }

    function bookbus()
    {
        $bookings = bustrip::all();
        return view('bookbus')->with('bookings',$bookings);
    }

    function addBustrips()
    {

        return view('addBustrips');
    }

    function regsubmit(Request $request)
    {
        return $request->all();
    }

    function postBusTrip(Request $request)
    {
        $this->validate($request, [
            "busname" => "required",
            "bustype" => "required",
            "image" => "required",/*|image|mimes:jpg,png*/
            "from" => "required",
            "to" => "required",
            "depdate" => "required|date",
            "arivaldate" => "required|date",
            "time" => "required",
            "rate" => "required|numeric",
            "contact" => "required|numeric",
        ]);

        $bustrip = new  bustrip($request->all());
        $path = $request->file('image')->store('img');
        $bustrip->image = substr($path, 4);
        if ($bustrip->save()) {
            $request->session()->flash('success','');
            return view('addBustrips');
        } else {
            return "Some thing happend";
        }

//        $bustrip = new
//        return $bustrip;
    }


    function  newbusbooking($id,Request $request){
        $user_bookings = Auth::user()->busbookings()->where('trip_id',$id)->get();
        if(count($user_bookings)!= 0){
            $request->session()->flash('duplicate','');
            return redirect()->route('bookbus');
        }
        $busBooking = new busbooking();
        $busBooking->trip_id = $id;
        if(Auth::user()->busbookings()->save($busBooking)){
            $request->session()->flash('success','');
            return redirect()->route('bookbus');
        }else{
            return "error";
        }
    }

    function searchBus(Request $request){
        $trips = bustrip::orWhere('from',$request->from)
            ->orWhere('to',$request->to)
            ->orWhere('depdate',$request->onwarddate)
            ->get();
        
        if(count($trips) == 0){
            return view('bookbus')->with('bookings',null);
        }else{
            return view('bookbus')->with('bookings',$trips);
        }
    }
}
