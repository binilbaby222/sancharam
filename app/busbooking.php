<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class busbooking extends Model
{
    protected $table ="bus_bookings";
    public function user()
    {
        return $this->belongsTO('App\User');
    }
    public function trip(){
        return $this->belongsTO('App\bustrip');
    }

    protected $fillable = [
        'trip_id',
        'user_id'
    ];
}
