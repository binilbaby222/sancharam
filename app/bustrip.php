<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bustrip extends Model
{
    protected $table ="bus_trips";
    protected $fillable = [
        'busname',
        'bustype',
        'from', 
        'to',
        'depdate',
        'arivaldate',
        'time',
        'rate', 
        'contact'
    ];

    public function bookings(){
        return $this->hasMany('App\busbooking');
    }
}
