<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('busname');
            $table->string('bustype');
            $table->string('from');
            $table->string('to');
            $table->string('depdate');
            $table->string('arivaldate');
            $table->string('time');
            $table->string('rate');
            $table->string('image');
            $table->string('contact');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_trips');
    }
}
