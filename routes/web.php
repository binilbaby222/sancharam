<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','UserController@home')
    ->name('home'); 

Route::get('/bookbus','UserController@bookbus')
    ->name('bookbus');

Route::get('/booking/bus/{id}','UserController@newbusbooking')
    ->name('newbusbooking');

Route::get('/admin/bustrip','UserController@addBustrips')
    ->name('addBustrips');


Auth::routes();
/*Route for home view*/
Route::get('/home', 'HomeController@index');

/*Route for getting admin view*/
Route::get('/admin', 'admincontroller@submitadmin')
->name('submitadmin');



/*Route for submiting bus trip by admin*/
Route::post('/admin/postbustrip','UserController@postBusTrip')
    ->name('postbustrip');


/*Route for handling the search query for buses*/
Route::get('/search/bus','UserController@searchBus')
    ->name('searchBus');

Route::get('/storage/images/{filename}', function ($filename)
{
  
    $path = storage_path() .  '/app/img/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
 })
    ->name('serveImage');
